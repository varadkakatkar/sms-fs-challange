const path = require('path');
const favicon = require('serve-favicon');
const compress = require('compression');
const helmet = require('helmet');
const cors = require('cors');
const logger = require('./logger');
const bodyParser = require('body-parser');
const express = require('@feathersjs/express');
const request = require('request');

const feathers = require('@feathersjs/feathers');
const configuration = require('@feathersjs/configuration');
const socketio = require('@feathersjs/socketio');
const rest = require('@feathersjs/express/rest');
const errors = require('@feathersjs/errors');

const webpack = require('webpack');
const webpackConfig = require('./webpack.config');
const compiler = webpack(webpackConfig);


const middleware = require('./middleware');
const services = require('./services');
const appHooks = require('./app.hooks');
const channels = require('./channels');

const authentication = require('./authentication');

const mongodb = require('./mongodb');
const serveStatic = require('@feathersjs/feathers').static;

const mongoose = require('./mongoose');

const app = express(feathers());

var apiUrl = app.get('apiUrl');

// Load app configuration
app.configure(configuration(path.join(__dirname, '..')));
// Enable security, CORS, compression, favicon and body parsing
app.use(helmet());
app.use(cors());
app.use(compress());
app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true, publicPath: webpackConfig.output.publicPath
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(favicon(path.join(app.get('public'), 'favicon.ico')));
// Host the public folder
// app.use('/', express.static(app.get('public')));
app.use('/css', express.static( app.get('public')+'/assets/css' ));
app.use('/js', express.static( app.get('public')+'/assets/js' ));
app.use('/images', express.static( app.get('public')+'/assets/images' ));
app.use('/fonts', express.static( app.get('public')+'/assets/fonts' ));

// Set up Plugins and providers
app.configure(express.rest());
app.configure(socketio());
app.configure(rest());

app.configure(mongodb);

app.configure(mongoose);

// Configure other middleware (see `middleware/index.js`)
app.configure(middleware);
app.configure(authentication);
// Set up our services (see `services/index.js`)
app.configure(services);
app.use('/', function(req, res) {
  if (req.path != '/' && (!req.headers.accept || req.headers.accept.indexOf('text/html') == -1)) {
    throw new errors.NotFound('The requested path does not exist');
  }
  res.sendFile(path.join(__dirname, '../client/index.html'));
});
// Set up event channels (see channels.js)
app.configure(channels);

// Configure a middleware for 404s and the error handler
app.use(express.notFound());
app.use(express.errorHandler({ logger }));

app.hooks(appHooks);

module.exports = app;
