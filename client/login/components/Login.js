import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import loginActions from '../actions/loginActions';
var Row = require('react-bootstrap/lib/Row');
var Col = require('react-bootstrap/lib/Col');
import { Form, Text } from 'react-form';

class Login extends Component {
  constructor(props) {
    super(props);
    this.credentials = {
      username: '',
      password: ''
    };
    this.state ={
      value:''
    };
    this.credentials = {};
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(values) {
    this.credentials.username=values.username;
    this.credentials.password=values.password;
    this.props.loginActions.loginUser(this.credentials);
  }

  render(){
    return (
      <div className="login-container">
      <div className="container">
        <Form
          onSubmit={(values) => {
            {this.handleSubmit(values);}
          }}
          validate={values => {
            const {username} = values;
            const {password} = values;
            return {
              username: !username ? 'username is required' : undefined,
              password: !password ? 'Password is required' : undefined
            };
          }}
        >
        {({submitForm}) => {
          return (
            <form onSubmit={submitForm} id="login-form" className="center-block">
              <div className="modal-dialog login-modal">
                <div className="modal-content login-content">
                  <div className="modal-body">
                    <Row className="row modal-row">
                      <Col className="col-md-12">
                        <center><img src="/images/default-logo.png" width="250px" className="image-alignment"/></center>
                      </Col>
                    </Row>
                    <Row className="row modal-row">
                      <Col className="col-md-12">
                        <div className="form-group">
                          <Text field="username" name="username" className="form-control" id="exampleInputEmail" placeholder="UserName" />
                        </div>
                      </Col>
                    </Row>
                    <Row className="row modal-row">
                      <Col className="col-md-12">
                        <div className="form-group">
                          <Text field="password" type="password" name="password" className="form-control" id="exampleInputPassword" placeholder="Password"/>
                        </div>
                      </Col>
                    </Row>
                    <Row className="row modal-row">
                      <Col className="col-md-12">
                        <div className="form-group">
                          <button className="btn btn-primary pull-right">Login</button>
                        </div>
                      </Col>
                    </Row>
                    <Row className="row modal-row">
                      <div className="modal-footer" style={{borderTop: 0}}></div>
                    </Row>
                  </div>
                </div>
              </div>
            </form>
          );
        }}
        </Form>
      </div>
        {/*<div className="login-footer"></div>*/}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {state};
}

function mapDispatchToProps(dispatch) {
  return {
    loginActions: bindActionCreators(loginActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);
