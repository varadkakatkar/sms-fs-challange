import axios from 'axios';
import {browserHistory} from 'react-router';

let actions = {

  loginUser: function(userCredentials) {
    console.log("INSIDE LoginAction.loginUser()");
    return (dispatch) => {
      const request = {
        method: 'post',
        url: '/authentication',
        data: {
          "strategy": "local",
          email : userCredentials.username,
          password : userCredentials.password
        },
        headers: {
          'accept': 'json',
          'Content-Type': 'application/json'
        }
      };
      axios(request).then(response => {
         console.log('Response received ==> ' + JSON.stringify(response.data));
        dispatch({
          type: 'LOGGED_IN',
          data: response.data
        });
        browserHistory.push('/dashboard');
      }, error => {
        console.log('Error logging in'+JSON.stringify(error,null,4));
      });
    };
  },

  logout: function() {
    return (dispatch) => {
      dispatch({
        type: 'LOGOUT'
      });
      browserHistory.push('/');
    };
  }
};

export default actions;
