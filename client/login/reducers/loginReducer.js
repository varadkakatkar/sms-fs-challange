import moment from 'moment';

const initialState = {
  user: {},
  session: {},
  token :""
};

let loginReducer = function(user = initialState, action) {
  switch (action.type) {
    case 'LOGGED_IN':
      console.log('action.data =================',action)
      return Object.assign({}, {
        // user: action.data.user
        token : action.data.accessToken
      });
    case 'LOGOUT':
      return Object.assign({}, initialState);
    case 'USER_CREATED':
      return Object.assign({}, initialState);
    default:
    return user;
  }
};

export default loginReducer;
