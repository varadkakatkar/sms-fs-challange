/*
 * MainContainer.js
 */

import React, { Component } from 'react';
export default class MainContainer extends Component {
  render() {
    return (
      <div>
        {this.props.children}
      </div>
    );
  }
}
