import React from 'react';
import { render } from 'react-dom';
//import store from '../redux/store'
import { Provider } from 'react-redux';
import { applyMiddleware, createStore } from 'redux';
import rootReducer from './reducers';
import thunk from 'redux-thunk';
import { Router, browserHistory } from 'react-router';
import routes from './routes';
//import Dashboard from './dashboard/components/Dashboard';
const store = createStore(rootReducer, applyMiddleware(thunk));

render(
  <Provider store={store}>
    <Router history={browserHistory} routes={routes} onUpdate={() => window.scrollTo(0, 0)}/>
  </Provider>,
  document.getElementById('app')
);
