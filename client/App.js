import React, { Component } from 'react';
import { connect } from 'react-redux';
//import { bindActionCreators } from 'redux';
import Navigation from './Navigation';
import Login from './login/components/Login';
//var Col = require('react-bootstrap/lib/Col');
import Fullscreen from "react-full-screen";
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFull: false,
    };
    this.goFull = this.goFull.bind(this)
  }

  goFull(){
    this.setState({ isFull: true });
  }
  render(){
    console.log('PROPS => ' + JSON.stringify(this.props.login, null, 3));
    if(this.props.login) {
      return (
        <div>
          <Navigation />
          {this.props.children}
        </div>
     );
    }
    else{
      return(
          <div>
            <Login />
          </div>
      );
    }
  }
}

function mapStateToProps(state) {
  return {login: state.login};
}

export default connect(mapStateToProps)(App);
