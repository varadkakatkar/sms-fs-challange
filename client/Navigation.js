import React, { Component } from 'react';
import { connect } from 'react-redux';

class Navigation extends Component {


  componentDidMount() {
    this.update = setInterval(() => {
      this.getDate();
    }, 1 * 10);
  }

  componentWillUnmount() {
    clearInterval(this.update);
  }

  getDate () {
    var options = { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };
    var timeoptions = { hour: '2-digit', minute: '2-digit'};
    var date = (new Date().toLocaleDateString('en-US', options)).toUpperCase();
    var time = (new Date().toLocaleTimeString('en-US', timeoptions)).toUpperCase();
    this.setState({date, time});
  };

  constructor(props) {
    super(props);
    this.state = {
      date: '',
      time: ''
    };
  }

  render() {
    const { date, time }  = this.state;
    return (
        <div className="">
          {/* <!----navbar----------------->*/}
                <nav className="navbar navbar-default" role="navigation">
                  <div className="container-fluid">
                   {/* <!-- Brand and toggle get grouped for better mobile display -->*/}
                    <div className="navbar-header">
                      <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                      </button>
                      <img src="/images/default-logo.png" width="250px" height="100px" className="logo-image"/>
                    </div>

                {/*  <!-- Collect the nav links, forms, and other content for toggling -->*/}
                  <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul className="nav navbar-nav navbar-padding pull-right">
                      <li>
                        <a href="#">
                          <i className="fas fa-calendar-alt"></i> &nbsp;&nbsp;
                          {date}</a>
                      </li>
                      <li>
                        <a href="#">
                          <i className="far fa-clock"></i>&nbsp;&nbsp;
                          {time}</a>
                      </li>
                      <li className="dropdown">
                        <a href="#" className="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"  data-close-others="false">
                          <i className="far fa-user"></i></a>
                        <ul className="dropdown-menu dropdown-menu-right" role="menu">
                          <li><a href="login"> Logout</a></li>
                        </ul>
                      </li>
                    </ul>
                    
                  {/*<ul className="pull-right"><h4>Welcome, TecRedi!</h4><h5>May 12, 2017</h5></ul>*/}
                </div>{/*<!-- /.navbar-collapse -->*/}
              </div>{/*<!-- /.container-fluid -->*/}
                  {/*<video autoPlay muted loop id="myVideo">*/}
                    {/*<source src="/images/obimex.mp4" type="video/mp4"></source>*/}
                  {/*</video>*/}
              </nav>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {state};
}


export default connect(mapStateToProps)(Navigation);
