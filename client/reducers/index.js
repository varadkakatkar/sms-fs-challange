import { combineReducers } from 'redux';
import sessionReducer from './sessionReducer';
import loginReducer from '../login/reducers/loginReducer';
import dashboardReducer from '../dashboard/reducers/dashboardReducer';

const rootReducer = combineReducers({
  session: sessionReducer,
  login: loginReducer,
  dashboard: dashboardReducer
});

export default rootReducer
