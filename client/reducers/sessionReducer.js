const initialState = {
    token: "",
    data: "",
};

let sessionReducer = function(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}

export default sessionReducer
