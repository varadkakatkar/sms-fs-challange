const initialState = {
  sqadata: {},
  tickerdata: {},
  cityData : []
};

let dashboardReducer = function(user = initialState, action) {
  switch (action.type) {
    case 'TICKER_DATA':
      return Object.assign( {}, user, {tickerdata: action.data});
    case 'DATA_RECEIVED':
      return Object.assign({}, user, {sqadata: action.data});

    case 'CITY_DATA':
      console.log("action.data ",action.data);
      return Object.assign({}, user, {cityData: action.data});
    default:
      return user;
  }
};

export default dashboardReducer;
