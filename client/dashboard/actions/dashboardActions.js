import axios from 'axios';
import {browserHistory} from 'react-router';

let actions = {

  retrieveTickerData: function(userCredentials) {
    return (dispatch) => {
      const request = {
        method: 'post',
        url: '/tickerdata',
        data: {
          username : userCredentials.username,
          password : userCredentials.password
        }
      };
      axios(request).then(response => {
        dispatch({
          type: 'TICKER_DATA',
          data:  response
        });
        browserHistory.push('/dashboard');
      }, error => {
        console.log('Error logging in'+JSON.stringify(error,null,4));
      });
    };
  },

// retrieveSQAData: function(userCredentials) {
//     return (dispatch) => {
//       const request = {
//         method: 'post',
//         url: '/sqadata',
//         data: {
//           username : userCredentials.username,
//           password : userCredentials.password
//         }
//       };
//       axios(request).then(response => {
//         dispatch({
//           type: 'DATA_RECEIVED',
//           data:  response
//         });
//         browserHistory.push('/dashboard');
//       }, error => {
//         console.log('Error logging in'+JSON.stringify(error,null,4));
//       });
//     };
//   },

retrieveData: function(token) {
  return (dispatch) => {
    const request = {
      method: 'get',
      url: '/data',
      headers: {
        'accept': 'json',
        'Content-Type': 'application/json',
        'Authorization': token
      },
      
      data: {
        $skip : 0,
        $limit : 10
      }
    };
    axios(request).then(response => {
      console.log('response ',response);
      dispatch({
        type: 'CITY_DATA',
        data:  response
      });
      // browserHistory.push('/dashboard');
    }, error => {
      console.log('Error getting data '+JSON.stringify(error,null,4));
    });
  };
},

};

export default actions;
