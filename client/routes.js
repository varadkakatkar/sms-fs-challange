import React from 'react';
import { Route, IndexRoute } from 'react-router';
import Container from './MainContainer';
import App from './App';
import Dashboard from './dashboard/components/Dashboard';
import Login from './login/components/Login';

const routes = (
    <Route path="/" component={Container}>
    <Route path='/login' component={Login} />
    <IndexRoute component={Login} />
    <Route component={App} >
      <Route path='/dashboard' component={Dashboard} />
    </Route>
  </Route>
);
export default routes;
